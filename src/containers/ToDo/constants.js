export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const CLEAR_COMPLETED = 'CLEAR_COMPLETED';
export const SWITCH_THEME = 'SWITCH_THEME';
export const EDIT_TODO = 'EDIT_TODO';
