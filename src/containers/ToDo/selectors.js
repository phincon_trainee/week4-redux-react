import { createSelector } from 'reselect';

import { initialState } from '@containers/ToDo/reducer';

const selectTodo = (state) => state.todo || initialState;

export const selectListTodo = createSelector(selectTodo, (state) => state.listToDo);
export const selectIsDarkMode = createSelector([selectTodo], (toDo) => toDo.isDarkMode);
