/* eslint-disable no-shadow */
/* eslint-disable no-case-declarations */
import { produce } from 'immer';
import { ADD_TODO, REMOVE_TODO, TOGGLE_TODO, SWITCH_THEME, CLEAR_COMPLETED, EDIT_TODO } from './constants';

export const initialState = {
  listToDo: [
    // { id: 1, text: 'to do 1', completed: false },
    // { id: 2, text: 'to do 2', completed: false },
    // { id: 3, text: 'to do 2', completed: false },
    // { id: 4, text: 'to do 2', completed: false },
    // { id: 5, text: 'to do 2', completed: false },
  ],
  isDarkMode: false,
};
export const storedKey = ['listToDo'];

// eslint-disable-next-line default-param-last
const todoReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case ADD_TODO:
        draft.listToDo = [...draft.listToDo, action.listToDo];
        break;
      case REMOVE_TODO:
        draft.listToDo = draft.listToDo.filter((todo) => todo.id !== action.id);
        break;
      case TOGGLE_TODO:
        const todo = draft.listToDo.find((todo) => todo.id === action.id);
        if (todo) {
          todo.completed = !todo.completed;
        }
        break;
      case CLEAR_COMPLETED:
        draft.listToDo = draft.listToDo.filter((todo) => !todo.completed);
        break;
      case SWITCH_THEME:
        return {
          ...state,
          isDarkMode: !state.isDarkMode,
        };
      case EDIT_TODO:
        const todoToEdit = draft.listToDo.find((todo) => todo.id === action.payload.id);
        if (todoToEdit) {
          todoToEdit.text = action.payload.newText;
        }
        break;
      default:
        return state;
    }
  });
export default todoReducer;
