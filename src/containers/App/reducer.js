import { produce } from 'immer';

import { SET_GREETINGS, SET_COUNTRY_LIST } from '@containers/App/constants';
import { light } from '@mui/material/styles/createPalette';

export const initialState = {
  greetings: 'Hi from web!',
  countryList: [],
  generalAdvice: {},
  theme: light,
};

export const storedKey = ['theme'];

// eslint-disable-next-line default-param-last
const appReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_GREETINGS:
        draft.greetings = action.greetings;
        break;
      case SET_COUNTRY_LIST:
        draft.countryList = action.countryList;
        break;
      // case SET_ADVICES:
      //   draft.generalAdvice = action.advice;
      //   break;
      // case SET_THEME:
      //   return {
      //     ...state,
      //     theme: action.theme,
      //   };
      default:
        break;
    }
  });

export default appReducer;
