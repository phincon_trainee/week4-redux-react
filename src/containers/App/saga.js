import { takeLatest, call, put } from 'redux-saga/effects';

import { GET_ADVICES } from '@containers/App/constants';
import { setAdvice } from '@containers/App//actions';
import { getAdvice } from '@domain/api';

export function* doGetGeneralAdvice() {
  try {
    const generalAdvice = yield call(getAdvice);
    // console.log(generalAdvice);
    if (generalAdvice) {
      yield put(setAdvice(generalAdvice.slip));
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
}
export default function* appSaga() {
  yield takeLatest(GET_ADVICES, doGetGeneralAdvice);
}
