import { FormattedMessage } from 'react-intl';

const AdviceGenerator = () => (
  <div>
    <FormattedMessage id="app_greeting" />
  </div>
);

export default AdviceGenerator;
