/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/button-has-type */
import { useEffect, useState } from 'react';
import devider from '@static/images/pattern-divider-desktop.png';
import { setTheme, getAdvice } from '@containers/App/actions';
import { selectGeneralAdvice, selectTheme } from '@containers/App/selectors';
import PropTypes from 'prop-types';
import { useDispatch, useSelector, connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import dice from '@static/images/icon-dice.png';

// import CasinoIcon from '@mui/icons-material/Casino';
import LightModeIcon from '@mui/icons-material/LightMode';
import NightsStayIcon from '@mui/icons-material/NightsStay';
import AcUnitIcon from '@mui/icons-material/AcUnit';
import LocalFireDepartmentIcon from '@mui/icons-material/LocalFireDepartment';
// import { Box, CircularProgress } from '@mui/material';

import classes from './style.module.scss';

const AdviceGenerator = ({ theme, generalAdvice }) => {
  const [rotate, setRotate] = useState(false);
  const dispatch = useDispatch();
  const handleTheme = (event) => {
    const name = event.currentTarget.getAttribute('name');
    dispatch(setTheme(name));
  };

  useEffect(() => {
    // Dispatch the getAdvice action on component mount
    dispatch(getAdvice());
  }, [dispatch]);

  const adviceData = useSelector((state) => state.app.generalAdvice);
  return (
    <div className={classes.App}>
      <div className={classes.btnGroup}>
        <button className={classes.themeBtn} id={classes.light} name="light" onClick={handleTheme}>
          <LightModeIcon />
        </button>
        <button className={classes.themeBtn} id={classes.dark} name="dark" onClick={handleTheme}>
          <NightsStayIcon name="dark" />
        </button>
        <button className={classes.themeBtn} id={classes.cold} name="cold" onClick={handleTheme}>
          <AcUnitIcon name="cold" />
        </button>
        <button className={classes.themeBtn} id={classes.warm} name="warm" onClick={handleTheme}>
          <LocalFireDepartmentIcon name="warm" />
        </button>
      </div>
      <div className={classes.content}>
        <h4 className={classes.head}>Advice #{adviceData?.id}</h4>
        <div className={classes.advice}>
          <p>“{adviceData?.advice}”</p>
        </div>
        <div className={classes.bottom}>
          <img src={devider} />
          <button
            className={classes.btn}
            onClick={() => {
              window.location.reload();
            }}
          >
            <img
              className={`${classes.dice} ${rotate ? classes.rotate : ''}`}
              src={dice}
              onClick={() => {
                setRotate(true);
              }}
              alt="Dice"
            />
          </button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
  generalAdvice: selectGeneralAdvice,
});

AdviceGenerator.propTypes = {
  theme: PropTypes.string,
  generalAdvice: PropTypes.object,
};

export default connect(mapStateToProps)(AdviceGenerator);
