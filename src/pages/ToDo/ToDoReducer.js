/* eslint-disable default-param-last */
const initialState = {
  isDarkMode: false,
  todos: [],
  filter: 'all',
};
const ToDoReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'TOGGLE_DARK_MODE':
      return {
        ...state,
        isDarkMode: !state.isDarkMode,
      };
    case 'ADD_TODO':
      return {
        ...state,
        todos: [...state.todos, action.payload],
      };
    // Similar cases for other actions
    default:
      return state;
  }
};

export default ToDoReducer;
