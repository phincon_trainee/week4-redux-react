import { Navigate } from 'react-router-dom';

const RedirectTodo = () => <Navigate to="/todo" />;

export default RedirectTodo;
